import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
// import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'dart:convert';
import 'second.dart';

void main() => runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    ));

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var teamName, teamLead, member1, member2, member3, college, contact, email;
  var databaseReference;
  String result = "Hey there !";
  var djs;

  Future _scanQR() async {
    try {
      String qrResult = await BarcodeScanner.scan();
      setState(() {
        result = qrResult;
      });
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          result = "Camera permission was denied";
        });
      } else {
        setState(() {
          result = "Unknown Error $ex";
        });
      }
    } on FormatException {
      setState(() {
        result = "You pressed the back button before scanning anything";
      });
    } catch (ex) {
      setState(() {
        result = "Unknown Error $ex";
      });
    }
  }

//   void createRecord(){
//   databaseReference.child("1").set({
//     'title': 'Mastering EJB',
//     'description': 'Programming Guide for J2EE'
//   });
//   databaseReference.child("2").set({
//     'title': 'Flutter in Action',
//     'description': 'Complete Programming Guide to learn Flutter'
//   });
// }

  Future getData() async {
    databaseReference
        .child('registration')
        .orderByChild('email')
        .equalTo(result)
        .once()
        .then((onValue) {
      Map data = onValue.value;
      print(data.values.toList()[0]);
      var js = json.encode(data.values.toList()[0]);
      djs = json.decode(js);
      print(djs["name"]);
      teamName = djs["team_name"];
      teamLead = djs["lead_name"];
      member1 = djs["member_1_name"];
      member2 = djs["member_2_name"];
      member3 = djs["member_3_name"];
      college = djs["college"];
      contact = djs["contact"];
      email = djs["email"];
    });

    // databaseReference.once().then((DataSnapshot snapshot) {
    //    print('Data : ${snapshot.value}');
    // });
  }

  @override
  void initState() {
    super.initState();
    databaseReference = FirebaseDatabase.instance.reference();
    print(databaseReference);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // centerTitle: true,
        leading: Image.asset("assets/dino_stand.png"),
        backgroundColor: Colors.redAccent,
        title: Text(
          "ERR_404 2.0",
          style: TextStyle(fontFamily: 'Oswald', color: Colors.black),
        ),
      ),
      body: Center(
        child: Container(
          child: Image.asset(
            "assets/dino.gif",
            width: 500.0,
            height: 250.0,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        icon: Icon(Icons.camera_alt),
        label: Text("Scan"),
        onPressed: () async {
          await _scanQR();
          await getData();
          Future.delayed(Duration(milliseconds: 3000), () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => SecondScreen(teamName, college, contact,
                    email, teamLead, member1, member2, member3)));
          });
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
