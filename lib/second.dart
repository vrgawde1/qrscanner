import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class SecondScreen extends StatefulWidget {
  var teamName, teamLead, member1, member2, member3, college, contact, email;

  SecondScreen(this.teamName, this.college, this.contact, this.email,
      this.teamLead, this.member1, this.member2, this.member3);

  @override
  _SecondScreenState createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  String s1, e, c, mem1, mem2, mem3;
  TextEditingController teamLeadController;
  TextEditingController m1;
  TextEditingController m2;
  TextEditingController m3;
  TextEditingController email;
  TextEditingController contact;
  final databaseReference = FirebaseDatabase.instance.reference();

  @override
  void initState() {
    super.initState();
    teamLeadController = new TextEditingController(text: widget.teamLead);
    email = new TextEditingController(text: widget.email);
    m1 = new TextEditingController(text: widget.member1);
    m2 = new TextEditingController(text: widget.member2);
    m3 = new TextEditingController(text: widget.member3);
    contact = new TextEditingController(text: widget.contact);
  }

  void createRecord() {
    databaseReference.child("attendance").push().set({
      'team_name': widget.teamName,
      'lead_name': teamLeadController.text,
      'college': widget.college,
      'contact': contact.text,
      'email': email.text,
      'member_1_name': m1.text,
      'member_2_name': m2.text,
      'member_3_name': m3.text,
    });
  }

  Widget textdisplay(x, y) {
    return TextFormField(
      style: TextStyle(
        color: Colors.redAccent,
        fontWeight: FontWeight.bold,
      ),
      initialValue: x,
      decoration: InputDecoration(
        labelText: y,
        labelStyle: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20.0,
          color: Colors.black,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text(
          'Registration',
          style: TextStyle(color: Colors.black, fontFamily: 'Oswald'),
        ),
      ),
      body: Container(
        child: ListView(
          padding: EdgeInsets.all(10.0),
          children: <Widget>[
            textdisplay(widget.teamName, 'Team Name'),
            textdisplay(widget.college, 'College'),
            Padding(
              padding: EdgeInsets.all(5.0),
            ),
            TextField(
              onChanged: (text) {
                setState(() {
                  c = contact.text;
                });
              },
              controller: contact,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: "Contact",
                labelStyle: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
            ),
            TextField(
              onChanged: (text) {
                setState(() {
                  e = email.text;
                });
              },
              controller: email,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: "Email",
                labelStyle: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
            ),
            Slidable(
              child: TextField(
                onChanged: (text) {
                  setState(() {
                    s1 = teamLeadController.text;
                    print("s1= " + s1);
                  });
                },
                controller: teamLeadController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Team Lead",
                  labelStyle: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              delegate: SlidableDrawerDelegate(),
              secondaryActions: <Widget>[
                IconSlideAction(
                  caption: 'Delete',
                  color: Colors.red,
                  icon: Icons.delete,
                  onTap: () {
                    setState(() {
                      teamLeadController.text = " ";
                      
                    });
                  },
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
            ),
            Slidable(
              child: TextField(
                onChanged: (text) {
                  setState(() {
                    mem1 = m1.text;
                  });
                },
                controller: m1,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Member 1",
                  labelStyle: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              delegate: SlidableDrawerDelegate(),
              secondaryActions: <Widget>[
                IconSlideAction(
                  caption: 'Delete',
                  color: Colors.red,
                  icon: Icons.delete,
                  onTap: () {
                    setState(() {
                      m1.text = " ";
                      
                    });
                  },
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
            ),
            Slidable(
              child: TextField(
                onChanged: (text) {
                  setState(() {
                    mem2 = m2.text;
                  });
                },
                controller: m2,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Member 2",
                  labelStyle: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              delegate: SlidableDrawerDelegate(),
              secondaryActions: <Widget>[
                IconSlideAction(
                  caption: 'Delete',
                  color: Colors.red,
                  icon: Icons.delete,
                  onTap: () {
                    setState(() {
                      m2.text = " ";
                      mem2 = m2.text;
                    });
                  },
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
            ),
            Slidable(
              child: TextField(
                onChanged: (text) {
                  setState(() {
                    mem3 = m3.text;
                  });
                },
                controller: m3,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Member 3",
                  labelStyle: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              delegate: SlidableDrawerDelegate(),
              secondaryActions: <Widget>[
                IconSlideAction(
                  caption: 'Delete',
                  color: Colors.red,
                  icon: Icons.delete,
                  onTap: () {
                    setState(() {
                      m3.text = " ";
                      mem3 = m3.text;
                    });
                  },
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.all(20.0),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                ButtonTheme(
                  minWidth: 150.0,
                  height: 45.0,
                  child: RaisedButton(
                    elevation: 15.0,
                    color: Colors.lightBlue,
                    child: Text(
                      'Submit',
                      style: TextStyle(fontFamily: 'Oswald'),
                    ),
                    onPressed: () {
                      print('It Works');
                      createRecord();
                      Navigator.pop(context);
                    },
                  ),
                ),
                ButtonTheme(
                  minWidth: 150.0,
                  height: 45.0,
                  child: RaisedButton(
                    elevation: 15.0,
                    color: Colors.redAccent,
                    child: Text(
                      'Cancel',
                      style: TextStyle(fontFamily: 'Oswald'),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
