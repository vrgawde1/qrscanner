# qrscanner

Attendance App developed for ERR_404 2.0 Hackathon of M.H. Saboo Siddik College of Engineering.
This app lets you take attendance of the participant by scanning the QR code provided to them.